<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class BankNumberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_number')->insert([
            'name' => '121',
            'amount' => 15000.0,
            'users_id' => 1
        ]);
        DB::table('bank_number')->insert([
            'name' => '122',
            'amount' => 2000.5,
            'users_id' => 2
        ]);
        DB::table('bank_number')->insert([
            'name' => '123',
            'amount' => 3000.0,
            'users_id' => 3
        ]);
    }
}
