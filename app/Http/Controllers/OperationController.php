<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\Operation;

class OperationController extends Controller
{

    /*
     * To put money into the account
     */
    public function addNewOperation (Request $req) {
        $operation = new Operation();
        $operation->name = $req->input('name');
        $operation->amount = $req->input('amount');
        $operation->banknumberfrom_id = $req->input('banknumberfrom_id');
        $operation->banknumberto_id = $req->input('banknumberto_id');
    }
}
